const express = require('express')
const app = express()
const port = 3000

app.post('/loadtest', (req, res) => {
    console.log(req.body)
    res.send(200)
})

app.listen(port, () => console.log(`Listening on port ${port}!`))