To run Locust using Python 3 (just omit the 3 from the python3 command if you're using an earlier version):

1. Create a virtual environment for the locust pip dependency in the root of your repo by running: `python3 -m venv venv`
2. Setup your terminal to use the virtual environment by running: `source venv/bin/activate`
3. Install Locust.io whilst your terminal is attached to the virtual environment using the `requirments.txt` file by running: `pip install -r  requirements.txt`
4. Update the `trunk_headers` dictionary in the `test_get_balance_journey.py` module with the Cognito auth headers. You can get them by logging into the Customer UI and viewing them in the developer tools.
5. To start the locust server run this command whilst the terminal is still running in your virtual environment: `locust -f load_test/test_get_balance_journey.py --host https://customer-ui-trunk.endeavour-dev.co.uk`
6. Open a browser and navigate to `http://localhost:8089` to start the locust test.