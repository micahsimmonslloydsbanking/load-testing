import json

from locust import HttpLocust, task, TaskSet


class GetBalanceJourney(TaskSet):
    trunk_headers: dict = {
        'authorization': None,
        'cookie': None
    }
    trunk_url = 'https://customer-ui-trunk.endeavour-dev.co.uk'
    customers_endpoint = f'{trunk_url}/api/customers'
    accounts_endpoint = f'{trunk_url}/api/accounts'
    transactions_endpoint = f'{trunk_url}/api/transactions'
    accounts_summary_endpoint = f'{trunk_url}/account/summary'
    incognito_endpoint = 'https://endeavour.auth.eu-west-2.amazoncognito.com/oauth2/token'

    def on_start(self):
        pass
        # uncomment method when we can get the trunk cookie header dynamically
        # self.populate_trunk_cookie_header()

    def populate_trunk_cookie_header(self):
        incognito_token_body = (
            'grant_type=authorization_code'
            '&scope=openid'
            '&response_type=code'
            '&client_id=None'
            f'&redirect_uri={self.trunk_url}/oauth2/idpresponse'
        )
        incognito_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = self.client.post(
            self.incognito_endpoint,
            data=incognito_token_body,
            headers=incognito_headers
        )
        # todo: populate the trunk_headers cookie field here

    @task(1)
    def task_get_balance(self):
        self.client.get(
            url=self.accounts_summary_endpoint,
            headers=self.trunk_headers
        )
        self.client.get(
            url=self.customers_endpoint,
            headers=self.trunk_headers
        )
        accounts_response = json.loads(
            self.client.get(url=self.accounts_endpoint, headers=self.trunk_headers).content
        )
        for account_id in self.extract_account_ids(accounts_response=accounts_response):
            transactions_query_string = {'account_id': account_id, 'page_size': '30'}
            self.client.get(
                url=f'{self.transactions_endpoint}/{account_id}',
                params=transactions_query_string,
                headers=self.trunk_headers
            )

    @staticmethod
    def extract_account_ids(*, accounts_response) -> [int]:
        return map(
            lambda account: account['identifiers']['account_id'],
            accounts_response['payload']['accounts']
        )


class GetBalanceUser(HttpLocust):
    task_set = GetBalanceJourney
    min_wait = 1000
    max_wait = 1000
